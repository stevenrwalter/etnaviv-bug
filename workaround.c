#include <stdint.h>
#include <stdio.h>
#include <assert.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/mman.h>

#include <drm.h>
#include <xf86drm.h>
#include <drm/etnaviv_drm.h>

int main(void)
{
	// Set 0x600 -> 0x630 (12 instances of VERTEX_ELEMENT_CONFIG) to 0
	uint32_t buffer[] = {
		0x080c0180 ,0x00000000 ,0x00000000 ,0x00000000,
		0x00000000 ,0x00000000 ,0x00000000 ,0x00000000,
		0x00000000 ,0x00000000 ,0x00000000 ,0x00000000,
		0, 0
	};
        struct drm_etnaviv_gem_submit submit_req = {
                .pipe = ETNA_PIPE_3D,
                .exec_state = ETNA_PIPE_3D,   
                .bos = 0,          
                .nr_bos = 0,      
                .relocs = 0,     
                .nr_relocs = 0,
                .pmrs = 0,    
                .nr_pmrs = 0,   
                .stream = (long long)(intptr_t)buffer,    
                .stream_size = sizeof(buffer), /* in bytes */
        };   
	int fd = open("/dev/dri/renderD128", O_RDWR);
	int rc;
	assert(fd > 0);

	rc = drmCommandWriteRead(fd, DRM_ETNAVIV_GEM_SUBMIT, &submit_req, sizeof(submit_req));
	assert(rc == 0);
}
