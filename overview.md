# GC400 Overview
At the highest level, the GPU has a command buffer in DRAM.  This
address is configured in the CMD_STREAM_BASE_ADDR register 0x640.  It
contains a sequence of commands, which are always at least 2 32-bit
words each.  Commands are always 64-bit aligned.  The most common
command is to update one or more internal GPU registers.  Annoyingly
these registers are not accessible any other way; they cannot be
directly accessed from the CPU, and there is no command for reading
their value.  Arguably the most important command is the "draw" command
that actually renders some sort of primitive.  Once a sequence of
commands has been placed in DRAM, the GPU is kicked off by setting the
enable bit and writing the number of command words to
FE_COMMAND_CONTROL 0x658.

The GPU also has its own MMU.  The MMU cannot be used for command
buffers, but it can be used for most other data accesses.  If the high
bit of an address is set (i.e. > 0x8000000) then the MMU is used for
address translation.  Note that even though the MMU allows physical
addresses greater than 32-bits, the high address lines are not
physically connected in the MV6270 and so all GPU memory must come from
the first 2GB.

## Data Flow
The basic draw command takes as arguments the type of primitives to draw
(triangles, for example) and the number of primitives to render.  For
basic TRIANGLES mode, this means the GPU needs 3x as many vertices as
primitives.  To get from vertex data in DRAM, the GPU will feed the
vertex data into the vertex shader, use the resulting data for primitive
assembly and rasterization, and then invoke the fragment shader for each
pixel to get the data for the final framebuffer output.  (A fragment can
be thought of as just a pixel; that's only not the case when doing
multi-sampling for e.g. anti-aliasing).

```mermaid
graph TD
C[MMU]
D(Vertex Data in DRAM)
E[Vertex Shader]
D -->|optionally| C
C -->|once per vertex| E
E --> H(Position and Varyings)
H --> F[Primitive Assembly]
F -->|once per pixel| G[Fragment Shader]
G --> I(Color and Depth in DRAM)

J(Vertex Program in SRAM) -->E
K(Fragment Program in SRAM) -->G
```

There are several registers involved in specifying what kind of data
goes into the vertex shader and where it comes from.  Most
fundamentally, VS_INPUT_COUNT 0x808 specifies the number of vertex
attributes the vertex shader expects.  For each attribute,
FE_VERTEX_ELEMENT_CONFIG 0x600+(4*i) specifies how to fetch and
interpret the vertex data.  This register specifies how many elements
the attribute has (i.e. for an X,Y,Z coordinate there would be three
elements), the type of the elements (i.e. FLOAT or INT or BYTE), and
which "vertex stream" is used to fetch the data.

The GC400 has four vertex streams.  Each stream has a BASE_ADDR register
(0x680 for the first stream) and a CONTROL register (0x684 for the first
stream).  The base address register is straight-forward, it is simply
the address of the first value in the stream.  If the high bit of the
address is set, then the MMU is used for address translation, which also
allows for vertex data to be non-contiguous.  The control register is
also relatively simple; it specifies the stride for the data.  For each
subsequent vertex, the stride is added to the base address to locate the
data for that vertex.

Returning to the FE_VERTEX_ELEMENT_CONFIG register, there are two more
values that are important for fetching vertex data, 8 bits of "start
offset" and 8 bits of "end offset".  These values are used to locate the
data for this attribute within the vertex data.  In total, the data for
a specific attribute for a specific vertex "i" lives at BASE_ADDR + i *
STRIDE + START_OFFSET.  Note that the number of bytes specified by start
offset and end offset should be consistent with the number of elements
and type of data configured by ELEMENT_CONFIG.  OpenGL does specify how
missing data should be synthesized, but it's not clear to me that the
GC400 will do that for you.

The output of the vertex shader is the vertex position and zero or more
"varying" values.  In the simplest case, a vertex shader can just take
position values as input and pass them straight through.  However, the
input to the vertex shader can be any type of information, not just
position.  Commonly color information is also specified for each vertex
and passed through by the vertex shader.

As an example, suppose you want to provide X, Y, and Z position as well
as R, G, B color for each vertex, FLOAT values all.  This would be
expressed as two vertex attributes, one for position and one for color.
Let's further suppose that all of this data is interleaved, i.e. it
exists in memory as X, Y, Z, R, G, B, X, Y, Z, R, G, B... This means
that only one stream is needed, and the stride will be 2 attributes * 3
elements/attribute * 4 bytes/element = 24.

The first element config register will be configured for 3 elements,
FLOAT type, with a start offset of 0 and an end offset of 12.  The
second element will also be configured for 3 elements, FLOAT type, with
a start offset of 12 and an end offset of 24.

(It seems that mesa does some sort of JIT optimization.  In the above
example, the data can be represented as a single vertex stream, and
that's how mesa treats it for the first couple of frames.  After 4 or 5
frames mesa switches to treating each attribute as its own stream, even
though they are interleaved and tightly packed.  It's not clear that
this improves performance on the GC400)

## What are shaders for?
As stated above, the vertex shader runs for each input vertex of a
primitive.  The only required output of the vertex shader is a position.
The vertex shader may also have additional outputs called "varyings" by
OpenGL.  They are so called because the GPU can automatically
interpolate the values for varyings between vertices.  Varyings are fed
as inputs to the pixel shader, which can then use the value for colors
or other purposes.  So suppose you had a triangle with a different value
to use for color at each vertex, the GPU would interpolate that value so
that you get a gradient of colors between each corner of the triangle.

The vertex shader is typically used for coordinate transformation and
lighting.  The input coordinates can be in any coordinate space, however
the output coordinate space is called "clip coordinates."  Clip
coordinates range from -1.0 to 1.0.  Clip coordinates are converted by
fixed-function parts of the GPU into "window coordinates."

The fragment shader then runs for each fragment (pixel, effectively) of
the output.  It receives as input the position within the framebuffer as
well as any varying outputs from the vertex shader.  The fragment shader
returns as output the value that gets stored in the framebuffer,
normally this is a color and the framebuffer is called a "color buffer."
The fragment shader is typically where a texture is applied, if the
primitive is textured.

## Shader Architecture
The vertex shader shader and fragment shader have similar architectures.
There are 16 local registers called temporaries.  Inputs and outputs for
the shaders are also stored in the temporary registers.  This is
controlled by the VS_OUPUT and VS_INPUT registers (0x810 - 0x830).
These registers contain 4-bit bitfields, 16 each for inputs and outputs.
Each bitfield controls the temporary register where the input is placed
at the beginning of shader execution, and from where the output is
retrieved at the end of shader execution.  VS_INPUT_COUNT 0x808 and
VS_OUTPUT_COUNT 0x804 control the number of inputs and outputs,
respectively, and therefore which bitfields in VS_OUTPUT and VS_INPUT
are actually used.

Each temporary register has four components, x, y, z, and w.  For matrix
types multiple temporaries must be used, e.g. a mat4 value would take 4
registers.  The components of each register that are used and how
depends on the opcode in question.

Additionally, there are "uniform" registers available for storing
constants and other values that do not change from one run of a shader
to the next.  These values are stored in GPU SRAM in registers starting
at 0x5000.  The GC400 reports a value of 168 for num_constants, so
uniforms 0x5000 through 0x52a0 are usable.

The OpenGL spec requires that vertex shaders are idempotent; that is
given the same inputs they will produce the same outputs.  Hardware
implementations are specifically allowed to keep a cache of vertex
shader results and re-use outputs rather than running the shader for
every vertex.  The GC400 reports a vertex_cache_size of 8, which I
interpret to mean that it can remember the outputs for up to 8 vertexes,
and will not re-run the vertex shader if it finds a cache hit for a
given set of inputs.

More details of the shader architecture such as instruction set can be
found in the following documents:

* https://github.com/laanwj/etna_viv/tree/master/doc/isa.md
* https://github.com/laanwj/etna_viv/blob/master/rnndb/isa.xml

## Command Buffer Chaining
The command buffer is actually more capable than the earlier discussion
illustrates.  It can be more than a single contiguous area of memory,
because there are commands for branching (CMD_LINK) to a different area
of memory.  The commands in the main command buffer (the one pointed to
by CMD_STREAM_BASE_ADDR) are only ever written by the kernel.  When
userspace submits a command buffer, it is copied into a separate area of
memory and verified.  Verification ensures that the GPU can't be used to
access or corrupt areas of memory that the user process is not allowed
to access.

If verification is successful, the new command buffer in chained into
main command buffer.  A link is added to the main command buffer to jump
to the user-submitted buffer, and a link is added to the end of the user
buffer to jump back to the end of the main command buffer.

Next there is a bit of cleverness to avoid starting and stopping the GPU
as much as possible.  When the user buffer jumps back to the main
command buffer, it will find a WAIT command followed by a LINK command
that jumps back to the wait.  If another command buffer is received by
the kernel, the kernel will first update the padding word after the WAIT
instruction to the address of the new user buffer, and then it will
update the opcode to be LINK instead of WAIT.  This will cause the GPU
to jump to the new user-submitted buffer the next time it loops around.
If the GPU happens to see the intermediate state, it will see a WAIT and
ignore the address in the subsequent word.

## References
* https://www.khronos.org/registry/OpenGL/specs/es/2.0/es_full_spec_2.0.pdf
* https://www.khronos.org/opengl/wiki/Rendering_Pipeline_Overview
* https://github.com/laanwj/etna_viv/tree/master/doc
* https://github.com/laanwj/etna_viv/blob/master/rnndb/isa.xml
* https://github.com/laanwj/etna_viv/blob/master/rnndb/state.xml