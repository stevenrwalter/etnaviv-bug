LDLIBS += -lEGL -lGLESv2 -lm
CFLAGS += `pkg-config --cflags libdrm`
LDLIBS += `pkg-config --libs libdrm gbm`

all: test

clean:
	rm -f test
	rm -f *.o
